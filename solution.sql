INSERT INTO users (email, password, datetime_created)
VALUES ('johnsmith@gmail.com', 'passwordA', '2020-01-01 01:00:00'),
('juandelacruz@gmail.com', 'passwordB', '2020-01-01 02:00:00'),
('janesmith@gmail.com', 'passwordC', '2020-01-01 03:00:00'),
('mariadelacruz@gmail.com', 'passwordD', '2020-01-01 04:00:00'),
('johndoe@gmail.com', 'passwordE', '2020-01-01 05:00:00');


INSERT INTO posts (title, content, datetime_posted, author_id)
VALUES ('First Code', 'Hello World!', '2020-01-01 01:00:00', 1),
('Second Code', 'Hello Earth', '2020-01-01 02:00:00', 1),
('Third Code', 'Welcome to Mars!', '2020-01-01 03:00:00', 2),
('Fourth Code', 'Bye bye solar system', '2020-01-01 04:00:00', 4);

